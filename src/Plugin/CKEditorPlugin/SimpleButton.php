<?php

namespace Drupal\ckeditor_simplebutton\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Simple Buttons" plugin, with a CKEditor.
 *
 * @CKEditorPlugin(
 *   id = "simplebutton",
 *   label = @Translation("Simple Buttons Plugin")
 * )
 */
class SimpleButton extends PluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('ckeditor_simplebutton') . '/js/plugins/simplebutton/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $iconImage = \Drupal::service('extension.list.module')->getPath('ckeditor_simplebutton') . '/js/plugins/simplebutton/images/simplebutton.png';

    return [
      'simplebutton' => [
        'label' => t('Add Simple Button'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
